package com.example.themovie.Control

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themovie.Model.APIService
import com.example.themovie.Model.objMovie
import com.example.themovie.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: MovieAdapter
    private val movieList = mutableListOf<objMovie>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val vibrator = applicationContext?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator


        binding.rvRecycler.setHasFixedSize(true)
        binding.rvRecycler.isDrawingCacheEnabled = true
        binding.rvRecycler.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_AUTO
        binding.srlRefresh.setOnRefreshListener {
            initRecycler()
            binding.srlRefresh.isRefreshing = false
        }
        binding.chipPlayingNow.setOnClickListener {
            vibrator.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE))
            initRecycler()
        }
        binding.chipMostPopular.setOnClickListener {
            vibrator.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE))
            initRecycler()
        }
        initRecycler()
    }

    private fun initRecycler() {
        playingNow()
        adapter = MovieAdapter(movieList,applicationContext)
        binding.rvRecycler.setItemViewCacheSize(movieList.size)
        binding.rvRecycler.layoutManager = LinearLayoutManager(this)
        binding.rvRecycler.adapter = adapter


    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun playingNow() {
        val url :String = if (binding.chipPlayingNow.isChecked){ "discover/movie?primary_release_date.gte=2021-01-15&primary_release_date.lte=2021-07-21&api_key=7b38fd1807caacf97bfd260c81c320cc"
        }else "discover/movie?sort_by=popularity.desc&api_key=7b38fd1807caacf97bfd260c81c320cc"

        CoroutineScope(Dispatchers.IO).launch {
            val call = getRetrofit().create(APIService::class.java)
                .getMovies(url)
            val movies = call.body()
            runOnUiThread {
                if (call.isSuccessful) {
                    val peliculas = movies?.results ?: emptyArray()
                    movieList.clear()
                    movieList.addAll(peliculas)
                    adapter.notifyDataSetChanged()
                } else {
                    error()
                }
            }

        }
    }

    private fun error() {
        Toast.makeText(this, "se produjo un error", Toast.LENGTH_SHORT).show()
    }
}