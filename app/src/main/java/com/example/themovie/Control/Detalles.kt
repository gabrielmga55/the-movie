package com.example.themovie.Control

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import com.example.themovie.R
import com.example.themovie.databinding.ActivityDetallesBinding

class Detalles : AppCompatActivity() {

    private lateinit var binding: ActivityDetallesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetallesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val vibrator = applicationContext?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        val bundle = intent.extras

        var detalle = bundle!!.getString("detalle")
        var tite = bundle!!.getString("title")
        var video = bundle!!.getBoolean("video",false)

        binding.tvDetalles.text = detalle
        binding.toolbar.title = tite
        binding.toolbar.setNavigationOnClickListener {
            vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
            onBackPressed()
        }


    }
}