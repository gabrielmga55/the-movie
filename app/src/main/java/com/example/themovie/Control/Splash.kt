package com.example.themovie.Control

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.themovie.R
import android.app.ActivityOptions

import android.content.Intent

class Splash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Thread.sleep(500)
        startActivity(Intent(this, MainActivity::class.java))
        this.finish()
    }
}