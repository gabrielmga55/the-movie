package com.example.themovie.Control

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.themovie.Model.objMovie
import com.example.themovie.databinding.ItemBinding
import com.squareup.picasso.Picasso

class MoviesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ItemBinding.bind(view)
    private val vibrator = itemView.context?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

    fun bind(item: objMovie, applicationContext: Context) {
        binding.language.text = item.original_language
        binding.fecha.text = item.release_date
        binding.popularidad.text = item.popularity.toString()
        binding.title.text = item.title
        Picasso.with(itemView.context).load("https://image.tmdb.org/t/p/original${item.poster_path}").into(binding.ivImagen)
        binding.cardView.setOnClickListener {
            vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
            val bundle = Bundle()
            bundle.putString("detalle" , item.overview)
            bundle.putString("title" , item.original_title)
            bundle.putBoolean("video" , item.video)
            applicationContext.startActivity(Intent(applicationContext,Detalles::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtras(bundle))


        }

    }
}