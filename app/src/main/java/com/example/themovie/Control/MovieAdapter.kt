package com.example.themovie.Control

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.themovie.Model.objMovie
import com.example.themovie.R


class MovieAdapter(val movie: MutableList<objMovie>, val applicationContext: Context):RecyclerView.Adapter<MoviesViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MoviesViewHolder(layoutInflater.inflate(R.layout.item,parent,false))
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {

        val item = movie[position]
        holder.bind(item,applicationContext)
    }

    override fun getItemCount(): Int = movie.size
}